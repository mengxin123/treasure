extends Node

class_name State


var object
var fsm

# 进入状态时 会执行
func enter():
	pass

func update(_delta):
	pass
	
func physics_update(_delta):
	pass

func exit():
	pass

func change_state(next_state):
	fsm.change_state(next_state)

