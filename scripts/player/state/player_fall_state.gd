extends PlayerBaseState

@onready var coyote_timer = $CoyoteTimer

func enter():
	play("Fall")

func physics_update(delta):
	move(delta, true)
	#检查是否跳跃
	if object.is_on_floor():
		if input.jump_buffer:
			change_state("jump")
		if input.x == 0:
			change_state("idle")
		else :
			change_state("run")
		



