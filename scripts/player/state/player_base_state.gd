extends State

class_name PlayerBaseState

var input:
	get: return object.input

func play(animation):
	object.sprite.play(animation)

func acceleration(delta, direction = input.x):
	# 空中速度
	var mult = Player.AIR_MULTIPLIER if not object.is_on_floor() else 1.0

	# direction 是专门用来提供方向， 速度方向
	object.velocity.x = move_toward(object.velocity.x, Player.MAX_SPEED * direction, Player.ACCELERATION * mult * delta)

func apply_gravity(delta):
	var g = Player.JUMP_GRAVITY if fsm.current_state == "jump" else Player.FALL_GRAVITY
	object.velocity.y = move_toward(object.velocity.y, Player.TERMINAL_VELOCITY, delta * g)

func move(delta, apply_g, update_direction = true, direction = input.x):
	acceleration(delta, direction)
	if apply_g:
		apply_gravity(delta)
	if update_direction:
		object.direction = direction
	
	object.move_and_slide()
