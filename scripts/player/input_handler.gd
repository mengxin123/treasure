extends Node

class_name InputHandler
@onready var jump_buffer_timer = $JumpBufferTimer

var x = 0
var jump_just_press = false
#持续按住
var jump_press = false
#用于记录玩家短时间内 跳跃信息
var jump_buffer:
    get:
        return not jump_buffer_timer.is_stopped()
    set(value):
        if value: jump_buffer_timer.start()
        else : jump_buffer_timer.stop()

#每帧调用
func update():
    x = Input.get_axis("btn_left", "btn_right")
    jump_just_press = Input.is_action_just_pressed("btn_jump")
    jump_press = Input.is_action_pressed("btn_jump")
